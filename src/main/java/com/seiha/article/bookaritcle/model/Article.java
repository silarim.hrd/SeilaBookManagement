package com.seiha.article.bookaritcle.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.sql.Timestamp;

public class Article {

    private int id;
    @NotEmpty
    private String title;
    private Category category;
    @NotBlank
    private String description;

    private Timestamp createdDate;
    @NotBlank
    private String author;
    private String thumbnail;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
    public Article(){}
    public Article(int id, @NotEmpty String title, Category category, @NotBlank String description, Timestamp createdDate, @NotBlank String author, String thumbnail) {
        this.id = id;
        this.title = title;
        this.category = category;
        this.description = description;
        this.createdDate = createdDate;
        this.author = author;
        this.thumbnail = thumbnail;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", category=" + category +
                ", description='" + description + '\'' +
                ", createdDate=" + createdDate +
                ", author='" + author + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                '}';
    }
}
