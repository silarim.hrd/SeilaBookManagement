package com.seiha.article.bookaritcle.service;

import com.seiha.article.bookaritcle.repository.ArticleRepository;
import com.seiha.article.bookaritcle.model.Article;
import com.seiha.article.bookaritcle.model.ArticleFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImplement implements ArticleService {

    @Autowired
    private ArticleRepository articleRepository;
    @Override
    public void insert(Article article) {
        articleRepository.insert(article);
    }

    @Override
    public Article findOne(int id) {
        return articleRepository.findOne(id);
    }

    @Override
    public List<Article> findAll() {
        return articleRepository.findAll();
    }

    @Override
    public void delete(int id) {
        articleRepository.delete(id);
    }
    @Override
    public void update(Article article) {
        articleRepository.update(article);
    }

    @Override
    public List<Article> findAllFilter(ArticleFilter filter) {
        return articleRepository.findAllFilter(filter);
    }
}
