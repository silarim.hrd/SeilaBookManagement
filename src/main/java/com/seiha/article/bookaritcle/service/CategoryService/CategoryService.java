package com.seiha.article.bookaritcle.service.CategoryService;

import com.seiha.article.bookaritcle.model.Category;

import java.util.List;

public interface CategoryService {
    List<Category> findAll();
    Category findOne(int id);
}
