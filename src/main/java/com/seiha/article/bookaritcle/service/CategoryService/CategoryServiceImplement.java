package com.seiha.article.bookaritcle.service.CategoryService;

import com.seiha.article.bookaritcle.repository.category.CategoryRepository;
import com.seiha.article.bookaritcle.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImplement implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category findOne(int id) {
        return categoryRepository.findOne(id);
    }
}
