package com.seiha.article.bookaritcle.repository.provider;

import com.seiha.article.bookaritcle.model.ArticleFilter;
import org.apache.ibatis.jdbc.SQL;

public class ArticleProvider {

    public String findAllFilter(ArticleFilter filter){
            return new SQL(){{
                SELECT("a.id, a.title, a.description, a.author, a.thumbnail, a.created_date, a.category_id, c.name");
                FROM("tb_articles a");
                INNER_JOIN("tb_categories c ON a.category_id = c.id");
                if (filter.getTitle() != null)
                    WHERE("a.title ILIKE '%' || #{title} || '%'");
                if (filter.getCate_id() != null)
                    WHERE("a.category_id = #{cate_id}");

                ORDER_BY("a.id ASC");
            }}.toString();
    }
}
